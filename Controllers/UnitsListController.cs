﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using _01._11._2020_Urok.Models;

namespace _01._11._2020_Urok.Controllers
{
    [EnableCors(origins: "http://127.0.0.1:5500", headers: "*", methods: "*")]
    public class UnitsListController : ApiController
    {
        [HttpPost]
        public IEnumerable<Unit> GetAllUnits()
        {
            return UnitsList.GetAllUnits();
        }

        [HttpPost]
        public bool DeleteUnitById([FromBody]int id)
        {
            return UnitsList.DeleteUnitById(id);
        }

        [HttpPost]
        public bool AddNewUnit([FromBody]Unit unit)
        {
            return UnitsList.AddNewUnit(unit);
        }

        [HttpPost]
        public bool UpdateUnitById([FromBody]Unit unit)
        {
            return UnitsList.UpdateUnitById(unit);
        }

        [HttpPost]
        public Unit GetUnitById([FromBody]int id)
        {
            return UnitsList.GetUnitById(id);
        }
    }
}
