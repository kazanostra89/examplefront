﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01._11._2020_Urok.Models
{
    public class Unit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Cost { get; set; }
    }
}