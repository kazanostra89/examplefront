﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace _01._11._2020_Urok.Models
{
    public static class UnitsList
    {
        private static int numberId;
        private static List<Unit> units;

        static UnitsList()
        {
            numberId = 4;

            units = new List<Unit>()
            {
                new Unit()
                {
                    Id = 1,
                    Name = "Скелет-воин",
                    Attack = 6,
                    Defense = 6,
                    Cost = 70
                },
                new Unit()
                {
                    Id = 2,
                    Name = "Зомби",
                    Attack = 5,
                    Defense = 5,
                    Cost = 125
                },
                new Unit()
                {
                    Id = 3,
                    Name = "Приведение",
                    Attack = 7,
                    Defense = 7,
                    Cost = 230
                },
            };
        }

        public static IEnumerable<Unit> GetAllUnits()
        {
            return units;
        }

        public static bool AddNewUnit(Unit unit)
        {
            unit.Id = numberId;

            units.Add(unit);

            numberId++;

            return true;
        }

        public static bool UpdateUnitById(Unit unit)
        {
            Unit updatedUnit = units.FirstOrDefault(item => item.Id == unit.Id);

            if (updatedUnit != null)
            {
                updatedUnit.Name = unit.Name;
                updatedUnit.Attack = unit.Attack;
                updatedUnit.Defense = unit.Defense;
                updatedUnit.Cost = unit.Cost;

                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool DeleteUnitById(int id)
        {
            return units.RemoveAll(item => item.Id == id) == 1;
        }

        public static Unit GetUnitById(int id)
        {
            return units.FirstOrDefault(item => item.Id == id);
        }

    }
}