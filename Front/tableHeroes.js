export let tableHeroes =
{
    data() {
        return {
            units: []
        }
    },
    template: `
    <div>
    <table class='table table-striped table-dark'>
        <thead>
            <tr>
                <th scope='col'>ИД</th>
                <th scope='col'>Имя</th>
                <th scope='col'>Атака</th>
                <th scope='col'>Защита</th>
                <th scope='col'>Стоимость</th>
                </tr>
        </thead>
        <tbody>
            <tr v-for="unit in units">
                <th scope='row'>{{unit.Id}}</th>
                <td>{{unit.Name}}</td>
                <td>{{unit.Attack}}</td>
                <td>{{unit.Defense}}</td>
                <td>{{unit.Cost}}</td>
            </tr>
        </tbody>
    </table>
    <button class="btn btn-primary" v-on:click="GetAllUnits">Загрузить юнитов</button>
    </div>`,
    methods: {
        GetAllUnits: async function ()
        {
            try
            {
                let response = await fetch("http://localhost:49366/api/UnitsList/GetAllUnits",
                    {
                        method: "POST",
                        headers:
                        {
                            accept: "application/json"
                        }
                    });
        
                if (response != null && response.ok === true)
                {
                    this.units= await response.json();
                }
                else
                {
                    window.alert("SERVER ERROR: " + response.status);
                }
        
            }
            catch (e)
            {
                window.alert(e.message);
            }
        }
    }
};