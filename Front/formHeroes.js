export let formHeroes =
{
    data() {
        return {
            inputFieldId: "",
            inputFieldName: "",
            inputFieldAttack: "",
            inputFieldDefense: "",
            inputFieldCost: "",
        }
    },
    template: `
    <div class="first">
        <div>
            <label>ИД:</label>
            <input type="number" v-model="inputFieldId" />
        </div>
        <div>
            <label>Имя:</label>
            <input type="text" v-model="inputFieldName" />
        </div>
        <div>
            <label>Атака:</label>
            <input type="number" v-model="inputFieldAttack" />
        </div>
        <div>
            <label>Защита:</label>
            <input type="number" v-model="inputFieldDefense" />
        </div>
        <div>
            <label>Стоимость:</label>
            <input type="number" v-model="inputFieldCost" />
        </div>

        <div class="first">
        <button class="btn btn-primary" v-on:click="AddNewUnit">Добавить нового юнита</button><br />
        <button class="btn btn-primary" v-on:click="UpdateUnitById">Обновить юнита по ИД</button><br />
        <button class="btn btn-primary" v-on:click="DeleteUnitById">Удалить юнита по ИД</button><br />
        <button class="btn btn-primary" v-on:click="GetUnitById">Показать юнита по ИД</button><br />
        </div>
    </div>`,
    methods: {
        CleaningFields: function () {
            this.inputFieldId = "",
            this.inputFieldName = "",
            this.inputFieldAttack = "",
            this.inputFieldDefense = "",
            this.inputFieldCost = "" },
        AddNewUnit: async function () {
            if (this.inputFieldName === "")
            {
                window.alert("Не заполнено поле - Имя");
                return;
            }
            if (this.inputFieldAttack === "")
            {
                window.alert("Не заполнено поле - Атака");
                return;
            }
            if (this.inputFieldDefense === "")
            {
                window.alert("Не заполнено поле - Защита");
                return;
            }
            if (this.inputFieldCost === "")
            {
                window.alert("Не заполнено поле - Стоимость");
                return;
            }

            let unit = {
                Name: this.inputFieldName,
                Attack: this.inputFieldAttack,
                Defense: this.inputFieldDefense,
                Cost: this.inputFieldCost
            };
        
            try
            {
                let response = await fetch("http://localhost:49366/api/UnitsList/AddNewUnit",
                    {
                        method: "POST",
                        headers:
                        {
                            accept: "application/json",
                            "Content-Type": "application/json;charset=utf-8"
                        },
                        body: JSON.stringify(unit)
                    });
        
                if (response.ok === true)
                {
                    let result = await response.json();
        
                    if (result == true)
                    {
                        window.alert(`Добавлен новый Юнит:\n` +
                                    `Имя: ${unit.Name}\n` +
                                    `Атака: ${unit.Attack}\n` +
                                    `Защита: ${unit.Defense}\n` +
                                    `Стоимость: ${unit.Cost}`);
                        this.CleaningFields();
                    }
                    else
                    {
                        window.alert("Юнит не добавлен");
                    }
                }
                else
                {
                    window.alert("SERVER ERROR: " + response.status);
                }
            }
            catch (e)
            {
                window.alert(e.message);
            }
            
        },
        DeleteUnitById: async function () {
            if (this.inputFieldId === "")
            {
                window.alert("Не заполнено поле - ИД");
                return;
            }

            try
            {
                let response = await fetch("http://localhost:49366/api/UnitsList/DeleteUnitById",
                    {
                        method: "POST",
                        headers:
                        {
                            accept: "application/json",
                            "Content-Type": "application/json;charset=utf-8"
                        },
                        body: JSON.stringify(this.inputFieldId)
                    });

                if (response.ok === true)
                {
                    let result = await response.json();

                    if (result == true)
                    {
                        window.alert(`Выбранный Юнит удален:\nИД: ${this.inputFieldId}`);
                        this.CleaningFields();
                    }
                    else
                    {
                        window.alert("Выбранный Юнит не удален");
                    }
                }
                else
                {
                    window.alert("SERVER ERROR: " + response.status);
                }
            }
            catch (e)
            {
                window.alert(e.message);
            }
        },
        UpdateUnitById: async function () {
            if (this.inputFieldId === "")
            {
                window.alert("Не заполнено поле - ИД");
                return;
            }
            if (this.inputFieldName === "")
            {
                window.alert("Не заполнено поле - Имя");
                return;
            }
            if (this.inputFieldAttack === "")
            {
                window.alert("Не заполнено поле - Атака");
                return;
            }
            if (this.inputFieldDefense === "")
            {
                window.alert("Не заполнено поле - Защита");
                return;
            }
            if (this.inputFieldCost === "")
            {
                window.alert("Не заполнено поле - Стоимость");
                return;
            }

            let unit = {
                Id: this.inputFieldId,
                Name: this.inputFieldName,
                Attack: this.inputFieldAttack,
                Defense: this.inputFieldDefense,
                Cost: this.inputFieldCost
            };

            try
            {
                let response = await fetch("http://localhost:49366/api/UnitsList/UpdateUnitById",
                    {
                        method: "POST",
                        headers:
                        {
                            accept: "application/json",
                            "Content-Type": "application/json;charset=utf-8"
                        },
                        body: JSON.stringify(unit)
                    });

                if (response.ok === true)
                {
                    let result = await response.json();

                    if (result == true)
                    {
                        window.alert(`Юнит под ИД ${unit.Id} и именем ${unit.Name} обнавлен!`);
                        this.CleaningFields();
                    }
                    else
                    {
                        window.alert(`Юнит под ИД ${unit.Id} и именем ${unit.Name} не обнавлен!`);
                    }
                }
                else
                {
                    window.alert("SERVER ERROR: " + response.status);
                }
            }
            catch (e)
            {
                window.alert(e.message);
            }
        },
        GetUnitById: async function () {
            if (this.inputFieldId === "")
            {
                window.alert("Не заполнено поле - ИД");
                return;
            }

            try
            {
                let response = await fetch("http://localhost:49366/api/UnitsList/GetUnitById",
                    {
                        method: "POST",
                        headers:
                        {
                            accept: "application/json",
                            "Content-Type": "application/json;charset=utf-8"
                        },
                        body: JSON.stringify(this.inputFieldId) 
                    });

                if (response.ok === true)
                {
                    let unit = await response.json();

                    if (unit != null)
                    {
                        window.alert(`Запрошенный юнит:\n` +
                                    `ИД: ${unit.Id}\n` +
                                    `Имя: ${unit.Name}\n` +
                                    `Атака: ${unit.Attack}\n` +
                                    `Защита: ${unit.Defense}\n` +
                                    `Стоимость: ${unit.Cost}`);

                        this.CleaningFields();
                    }
                    else
                    {
                        window.alert("Юнит не найден");
                    }
                }
                else
                {
                    window.alert("SERVER ERROR: " + response.status);
                }
            }
            catch (e)
            {
                window.alert(e.message);
            }
        }
    }
}