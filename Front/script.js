import {tableHeroes} from "./tableHeroes.js";
import {formHeroes} from "./formHeroes.js";

let vm = new Vue({
    el: "#tH",
    components: {
        "table-heroes": tableHeroes,
        "form-heroes": formHeroes
    }
});